import { v4 as generate_id } from 'uuid'; 

export class DistributionLinux{

    private id_distribution: string;
    private name: string;
    private description:string;
    private summary:string;
    private official_url:string;
    private architecture: string;
    private logo: string;

    public constructor({name_distribution,description,official_url,summary,architecture,logo}:any){
        this.id_distribution = generate_id();
        this.name = name_distribution;
        this.description = description;
        this.architecture = architecture;
        this.official_url = official_url;
        this.summary = summary;
        this.logo = logo;
    }

    public getIdDistribution(): string {
        return this.id_distribution;
    }
    public setIdDistribution(value: string) {
        this.id_distribution = value;
    }

    public getName(): string {
        return this.name;
    }
    public setName(value: string) {
        this.name = value;
    }

    public getArchitecture(): string {
        return this.architecture;
    }
    public setArchitecture(value: string) {
        this.architecture = value;
    }

    public setDescription(description:string):void{
        this.description = description;
    }
    public getDescription():string{
        return this.description;
    }

    public setUrl(oficcial_url:string):void{
        this.official_url = oficcial_url;
    }
    public getUrl():string{
        return this.official_url;
    }

    public getLogo(): string {
        return this.logo;
    }
    public setLogo(value: string) {
        this.logo = value;
    }

    public getSummary():string{
        return this.summary;
    }

}