import { v4 as generate_id } from 'uuid';

export class Comentary{

    private id_comentary:string;
    private content: string;
    private date_publication: string;
    private id_user:string;
    private id_post:string;

    public constructor({ content,date_publication,id_post }){
        this.id_comentary = generate_id();
        this.content = content;
        this.date_publication = date_publication;
        this.id_post = id_post;
    }

    public setIdComentary(id_comentary:string):void{
        this.id_comentary = id_comentary;
    }
    public getIdComentary():string{
        return this.id_comentary;
    }

    public setIdPost(id_post:string):void{
        this.id_post = id_post;
    }

    public getIdPost():string{
        return this.id_post;
    }

    public getContent(): string {
        return this.content;
    }
    public setContent(value: string) {
        this.content = value;
    }

    public getDatePublication(): string {
        return this.date_publication;
    }
    public setDatePublication(value: string) {
        this.date_publication = value;
    }

    public setIdUser(id_user:string):void{
        this.id_user = id_user;
    }
    public getIdUser():string {
        return this.id_user;
    }
}