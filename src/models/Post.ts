import { v4 as generate_id } from 'uuid';

export class Post{

    private id_post:string;
    private title: string;
    private content: string;
    private date_publication: string;
    private id_user:string;

    public constructor({ title,id_user,content }){
        this.id_post = generate_id();
        this.title = title;
        this.content = content;
        this.id_user = id_user;
    }

    public setIdPost(id_post:string):void{
        this.id_post = id_post;
    }

    public getIdPost():string{
        return this.id_post;
    }

    public getTitle(): string {
        return this.title;
    }
    public setTitle(value: string) {
        this.title = value;
    }

    public getContent(): string {
        return this.content;
    }
    public setContent(value: string) {
        this.content = value;
    }

    public getDatePublication(): string {
        return this.date_publication;
    }
    public setDatePublication(value: string) {
        this.date_publication = value;
    }

    public setIdUser(id_user:string):void{
        this.id_user = id_user;
    }
    public getIdUser():string {
        return this.id_user;
    }

}