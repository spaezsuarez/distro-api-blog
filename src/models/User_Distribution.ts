
export class UserDistributionRelationship{

    private id_distribution: string;
    private id_user:string;
    private opinion: string;

    public constructor({ id_distribution,opinion }){
        this.id_distribution = id_distribution;
        this.opinion = opinion;
    }

    public getOpinion(): string {
        return this.opinion;
    }
    public setOpinion(value: string) {
        this.opinion = value;
    }

    public getIdDistribution(): string {
        return this.id_distribution;
    }
    public setIdDistribution(value: string) {
        this.id_distribution = value;
    }

    public setIdUser(id_user:string):void{
        this.id_user = id_user;
    }

    public getIdUser():string {
        return this.id_user;
    }
}