import { v4 as generate_id } from 'uuid'; 
export class User{

    private id_user:string;
    private username:string;
    private email:string;
    private password_blog:string;

    public constructor({username,email,password_blog}:any){
        this.id_user = generate_id();
        this.username = username;
        this.email = email;
        this.password_blog = password_blog;
    }

    public setIdUser(id_user:string):void{
        this.id_user = id_user;
    }
    public getIdUser():string {
        return this.id_user;
    }

    public getName():string{
        return this.username;
    }
    public getEmail():string{
        return this.email;
    }

    public getPassword():string{
        return this.password_blog;
    }
    public setPassword(password:string):void{
        this.password_blog = password;
    }
}