export class APIResponseDTO<T>{

    private data:T;
    private status:boolean;
    private message: string;

    public getMessage(): string {
        return this.message;
    }
    public setMessage(value: string) {
        this.message = value;
    }

    public setData(data:T):void{
        this.data = data;
    }

    public setState(state:boolean):void{
        this.status = state;
    }

    public getData():T{
        return this.data;
    }

    public getState():boolean{
        return this.status;
    }

    public setSuccesQuery(data:T,message:string):void{
        this.setState(true);
        this.setData(data);
        this.setMessage(message);
    }

    public setFailQuery(error:T):void{
        this.setState(false);
        this.setData(error);
    }

}