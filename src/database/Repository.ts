import { poolDabaseConection } from '../config/database_config';
import { Pool } from 'pg';

export abstract class Repository<T>{

    protected pool:Pool;

    public constructor(){
        this.pool = poolDabaseConection;
    }

    public sendQuery(query:string,params:any[]):Promise<any>{
        return new Promise((resolve,reject) => {
            this.pool.query(query,params, (err,results) => {
                if(err){
                    reject(err);
                }else{
                    resolve(results);
                }
            })
        });
    }

    public abstract save(data:T):Promise<any>;

    public abstract findBy(identifier:string,value:any):Promise<T>;

    public abstract getAll():Promise<T[]>;

    public abstract update(data:T,id:any):Promise<any>;
}