import { UserDistributionRelationship } from '../models/User_Distribution';
import { Repository } from './Repository';
import { mapUserDistributionRelations } from '../util/processData';

export class UserLinuxRelationShipRepository extends Repository<UserDistributionRelationship>{
    
    public save(data: UserDistributionRelationship): Promise<any> {
        return this.sendQuery(`INSERT INTO UserBlog_LinuxDistribution VALUES($1,$2,$3) RETURNING *;`,
        [data.getIdUser(),data.getIdDistribution(),data.getOpinion()]);
    }

    public findBy(identifier: string, value: any): Promise<UserDistributionRelationship> {
        return this.sendQuery(`SELECT * FROM UserBlog_LinuxDistribution WHERE LinuxDistribution.${identifier} = $1`,
        [value]).then((result) => {
            let responseData:UserDistributionRelationship;
            responseData = new UserDistributionRelationship({...result.rows[0]});
            return responseData;
        }).catch((err) => {
            return null;
        });
    }

    public getAll(): Promise<UserDistributionRelationship[]> {
        return this.sendQuery('SELECT * FROM UserBlog_LinuxDistribution;',[])
        .then((results) => {
            return mapUserDistributionRelations(results.rows);
        }).catch(() => []);
    }

    public update(data: UserDistributionRelationship, id: any): Promise<any> {
        return this.sendQuery(`UPDATE public.userblog_linuxdistribution
        SET opinion=$1 WHERE id_user=$2 AND id_distribution=$3;
        `,[data.getOpinion(),data.getIdUser(),id]);
    }

}