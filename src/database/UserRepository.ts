import { User } from '../models/User';
import { Repository } from './Repository';
import { mapUsers } from '../util/processData';

export class UserRepository extends Repository<User>{

    public save(data: User): Promise<any> {
        return this.sendQuery(
            `INSERT INTO UserBlog (id_user,username,email,password_blog) VALUES ($1,$2,$3,$4) RETURNING *;`,
            [data.getIdUser(),data.getName(),data.getEmail(),data.getPassword()]
        );
    }

    public findBy(identifier: string, value: any): Promise<User> {
        return this.sendQuery(`SELECT * FROM UserBlog WHERE UserBlog.${identifier} = $1`,[value])
        .then((response) => {
            const userResponse:User = new User({...response.rows[0]});
            userResponse.setIdUser(response.rows[0].id_user);
            return userResponse;
        }).catch((err) => {
            return null;
        });
    }

    public getAll(): Promise<User[]> {
        return this.sendQuery('SELECT * FROM UserBlog;',[]).then((response) => {
            return mapUsers(response.rows);
        }).catch(() => []);
    }
    
    public update(data: User, id: any): Promise<any> {
        return this.sendQuery(
            'UPDATE UserBlog set name=$1,password=$2 WHERE id_user=$3 RETURNING *;;',
            [data.getName(),data.getPassword(),id]
        );
    }
    
}