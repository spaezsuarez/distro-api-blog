import { Comentary } from '../models/Comentary';
import { mapComentaries } from '../util/processData';
import { Repository } from './Repository';

export class ComentaryRepository extends Repository<Comentary>{
    
    public save(data: Comentary): Promise<any> {
        return this.sendQuery('INSERT INTO Comentary VALUES($1,$2,now(),$3,$4) RETURNING *;',
            [data.getIdComentary(),data.getContent(),data.getIdPost(),data.getIdUser()]
        );
    }
    public findBy(identifier: string, value: any): Promise<Comentary> {
        return this.sendQuery(`SELECT * FROM Post WHERE Post.${identifier} = $1`,
            [value]
        ).then((result) => {
            const comentaryResponse:Comentary = new Comentary({...result.rows[0]});
            comentaryResponse.setIdUser(result.rows[0].id_user);
            comentaryResponse.setIdComentary(result.rows[0].id_comentary);
            return comentaryResponse;
        }).catch(() => null);
    }
    public getAll(): Promise<Comentary[]> {
        return this.sendQuery('SELECT * FROM Comentary;',[])
            .then((results) => {
                return mapComentaries(results);
            }).catch(() => []);
    }
    public update(data: Comentary, id: any): Promise<any> {
        return this.sendQuery('UPDATE Comentary set comentary=$1 where id_comentary=2',
            [data.getContent(),id]
        );
    }

}