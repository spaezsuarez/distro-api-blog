import { Post } from '../models/Post';
import { Repository } from './Repository';
import { mapPosts } from '../util/processData';
export class PostRepository extends Repository<Post>{

    public save(data: Post): Promise<any> {
        return this.sendQuery('INSERT INTO Post VALUES($1,$2,$3,now(),$4) RETUNRNING *;',
        [data.getIdPost(),data.getTitle(),data.getContent(),data.getIdUser()]);
    }
    
    public findBy(identifier: string, value: any): Promise<Post> {
        return this.sendQuery(`SELECT * FROM Post WHERE Post.${identifier} = $1`,[value])
        .then((results) => {
            const postResponse:Post = new Post({...results.rows[0]});
            postResponse.setIdPost(results.rows[0].id_post);
            return postResponse;
        }).catch(() =>  null);
    }
    
    public getAll(): Promise<Post[]> {
        return this.sendQuery('SELECT * FROM Post;',[]).then((results) => {
            return mapPosts(results);
        }).catch(() => []);
    }

    public update(data: Post, id: any): Promise<any> {
        throw new Error('Method not implemented.');
    }

}