import { Repository } from './Repository';
import { DistributionLinux } from '../models/DistributionLinux';
import { mapDistributions } from '../util/processData';

export class LinuxDistributionRepository extends Repository<DistributionLinux>{

    public save(data: DistributionLinux): Promise<any> {
       return this.sendQuery(
            `INSERT INTO LinuxDistribution VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING *;`,
            [data.getIdDistribution(),data.getName(),data.getDescription(),data.getSummary(),data.getUrl(),data.getArchitecture(),data.getLogo()]
        );
    }

    public findBy(identifier: string, value: any): Promise<DistributionLinux | null > {
        return this.sendQuery(`SELECT * FROM LinuxDistribution WHERE LinuxDistribution.${identifier} = $1`,[value])
        .then((response) => {
            const distroResponse:DistributionLinux = new DistributionLinux({...response.rows[0]});
            distroResponse.setIdDistribution(response.rows[0].id_distribution);
            return distroResponse;
        }).catch((err) => {
            console.error(err);
            return null;
        });
    }

    public getAll(): Promise<DistributionLinux[]> {
        return this.sendQuery('SELECT * FROM LinuxDistribution;',[]).then((response) => {
            return mapDistributions(response.rows);
        }).catch(() => []);
    }
    
    public update(data: DistributionLinux, id: any): Promise<any> {
        return this.sendQuery(
            `UPDATE LinuxDistribution set name_distribution=$1,architecture=$2,logo=$3 WHERE id_distribution=$4 RETURNING *;`,
            [data.getName(),data.getArchitecture(),data.getLogo(),id]
        );
    }

    
}