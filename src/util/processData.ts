import { User } from '../models/User';
import { DistributionLinux } from '../models/DistributionLinux';
import { UserDistributionRelationship } from '../models/User_Distribution';
import { Post } from '../models/Post';
import { Comentary } from '../models/Comentary';

export const mapUsers = (data:any[]):User[] => {
    const response:User[] = [];
    data.forEach((element:any) =>  {
        const tempData:User = new User({...element});
        tempData.setIdUser(element.id_user);
        response.push(tempData);
    });
    return response;
}

export const mapDistributions = (data:any[]):DistributionLinux[] => {
    const response:DistributionLinux[] = [];
    data.forEach((element:any) =>  {
        const distroResponse:DistributionLinux = new DistributionLinux({...element});
        distroResponse.setIdDistribution(element.id_distribution);
        response.push(distroResponse);
    });
    return response;
}

export const mapUserDistributionRelations = (data:any[]):UserDistributionRelationship[] => {
    const response:UserDistributionRelationship[] = [];
    data.forEach((element:any) =>  {
        response.push(new UserDistributionRelationship({...element}));
    });
    return response;
}

export const mapPosts = (data:any[]):Post[] => {
    const response:Post[] = [];
    data.forEach((element:any) => {
        const tempData:Post = new Post({...element});
        tempData.setIdPost(element.id_post);
        response.push(tempData);
    });
    return response;
}

export const mapComentaries = (data:any[]):Comentary[] => {
    const response:Comentary[] = [];
    data.forEach((element:any) => {
        const tempData:Comentary = new Comentary({...element});
        tempData.setIdComentary(element.id_comentary);
        tempData.setIdUser(element.id_user);
        response.push(tempData);
    });
    return response;
    
}