import { UserLinuxRelationShipRepository } from '../database/UserLinuxRelationShipRepository';
import { UserRepository } from '../database/UserRepository';
import { User } from '../models/User';

export const obfuscate = (data:string, freeCharacters:number = 0):string => {
    let result:string = "";
    for (let i = 0; i < data.length; i++) {
        if (i < data.length - freeCharacters) {
            result += "*";
        } else {
            result += data.substring(i, i + 1);
        }
    }
    return result;
}

export const getUserOpinion = async (relathionShipRepository:UserLinuxRelationShipRepository,id_user:string,id_distribution:any):Promise<any> => {
    const query:string = 'SELECT opinion FROM public.userblog_linuxdistribution WHERE id_user=$1 and id_distribution=$2;';

    return await relathionShipRepository.sendQuery(query,[id_user,id_distribution])
        .then((result) =>  {
            if(result.rows.length !== 0){
                return {
                    'isEnabled':true,
                    'opinion':String(result.rows[0].opinion),
                };
            }else{
                return {
                    'isEnabled':false,
                    'opinion':'',
                };
            }
        }).catch(err => null);
}

export const getDistributionOpinions = async (
        relathionShipRepository:UserLinuxRelationShipRepository,
        userRepository:UserRepository,
        id_distribution:any):Promise<any[]> => {

    const response:any = {
        'opinions':[],
    };
    
    const queryOpinions:string = 'SELECT * FROM UserBlog_LinuxDistribution WHERE UserBlog_LinuxDistribution.id_distribution = $1;'
    const relationShipData:any[] = await relathionShipRepository.sendQuery(queryOpinions,[id_distribution]).then((results) => {
        return results.rows;
    }).catch(() => {
        return null;
    });

    if(relationShipData !== null && relationShipData.length !== 0){
        for(let i = 0; i < relationShipData.length; i++){
            const temp:User = await userRepository.findBy('id_user',relationShipData[i].id_user);
            response.opinions.push({
                'id_user':temp.getIdUser(),
                'user':temp.getName(),
                'opinion':relationShipData[i].opinion
            });
        }
    }
    return response;

}