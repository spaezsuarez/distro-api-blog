import { Application } from 'express';
import { setRoutes } from '../routes/router';
import { startDatabaseConnection } from './database_config';
import { initLogger,serverLogger } from './logger_config';

export const initServer = (server:Application,PORT:number):void => {
    //Handle Data accepted by the API
    initLogger();
    startDatabaseConnection();
    setRoutes(server);

    server.listen(PORT,() => {
        serverLogger.info(`Servidor corriendo en http://localhost:${PORT}`);
    });
}