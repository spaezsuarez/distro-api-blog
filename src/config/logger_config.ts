import { createLogger, Logger, format, transports } from 'winston';
import { resolve } from 'path';

export let serverLogger: Logger;

export const initLogger = (): void => {

    if (process.env.NODE_ENV === 'dev') {
        serverLogger = createLogger({
            level: 'info',
            format: format.combine(
                format.colorize(),
                format.printf(info => `[${new Date().toLocaleString()}] ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.Console()
            ],
        });
    } else {
        serverLogger = createLogger({
            level: 'info',
            format: format.combine(
                format.colorize(),
                format.printf(info => `[${new Date().toLocaleString()}] ${info.message}`)
            ),
            defaultMeta: { service: 'user-service' },
            transports: [
                new transports.File({filename: resolve('info.log')})
            ],
        });
    }

}