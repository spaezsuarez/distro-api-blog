import { Pool } from 'pg';
import { serverLogger } from '../config/logger_config';

export const poolDabaseConection:Pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.DB_PORT),
});

export const startDatabaseConnection = () =>  {
    poolDabaseConection.connect().then(() => {
        serverLogger.info('Conexión con base de datos realizada');
    }).catch((error) => {
        serverLogger.error(error);
    });
}