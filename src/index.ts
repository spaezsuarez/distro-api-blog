import express,{ Application } from 'express';
import { initServer } from './config/server_config';

const server:Application = express();
const PORT:number = parseInt(process.env.PORT) || 3000;

initServer(server,PORT);