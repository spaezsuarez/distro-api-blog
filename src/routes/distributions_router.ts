import { Router,Request,Response } from 'express';
import { userAuthenticationMiddleware } from '../middlewares/user_middleware';
import { relationShipCreationMiddleware } from '../middlewares/creation_middlewares';
import { UserDistributionRelationship } from '../models/User_Distribution';
import { APIResponseDTO } from '../models/APIResponseDTO';
import { UserLinuxRelationShipRepository } from '../database/UserLinuxRelationShipRepository';
import { UserRepository } from '../database/UserRepository';
import { serverLogger } from '../config/logger_config';
import { getUserOpinion,getDistributionOpinions } from '../util/util';

export const relation_ship:Router = Router();
const relathionShipRepository:UserLinuxRelationShipRepository = new UserLinuxRelationShipRepository();
const userRepository:UserRepository = new UserRepository();

relation_ship.post('/add-favorite',userAuthenticationMiddleware,
    (request:Request,response:Response) => {
        const apiResponse:APIResponseDTO<any> = new APIResponseDTO();
        let asociation:UserDistributionRelationship;
        asociation = new UserDistributionRelationship({...request.body});
        asociation.setIdUser(request.body.id_user);

        serverLogger.info(`Inicia servicio agregar lista linux_id:${asociation.getIdDistribution()}`);
        serverLogger.info(`user:${asociation.getIdUser()} `);

        relathionShipRepository.save(asociation).then((result) => {
            serverLogger.info('Distribución agregada exitosamente');
            apiResponse.setSuccesQuery(result.rows[0],'Distribución agregada exitosamente');
            response.status(201).json(apiResponse);
        }).catch((err) => {
            serverLogger.error(`Error:${err.message}`);
            apiResponse.setFailQuery(err);
            apiResponse.setMessage(err.message);
            response.status(500).json(apiResponse);
        });
    }
);

relation_ship.get('/opinion',userAuthenticationMiddleware,async (request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<Promise<string>> = new APIResponseDTO();
    const { id_user } = request.body;
    const { id_distribution } = request.query;

    const dataResponse:Promise<any> = await getUserOpinion(relathionShipRepository,id_user,id_distribution);

    if(dataResponse !== null){
        apiResponse.setSuccesQuery(dataResponse,'Data Founded');
        response.status(200).json(apiResponse);
    }else{
        apiResponse.setFailQuery(dataResponse);
        apiResponse.setMessage('Data Not Found');
        response.status(404).json(apiResponse);
    }
});

relation_ship.put('/opinion',userAuthenticationMiddleware,relationShipCreationMiddleware,(request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<any> = new APIResponseDTO();
    relathionShipRepository.update(request.body.relation_ship,request.body.relation_ship.getIdDistribution()).then((result) => {
        serverLogger.info('Opinión actualizada');
        apiResponse.setSuccesQuery(result.rows[0],'Opinión actualizada');
        response.status(201).json(apiResponse);
    }).catch((err) => {
        serverLogger.error(`Error:${err.message}`);
        apiResponse.setFailQuery(err);
        apiResponse.setMessage(err.message);
        response.status(500).json(apiResponse);
    });
});

//Endpoint for get Opinions
relation_ship.get('/distribution-opinions',userAuthenticationMiddleware,async (request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<any[]> = new APIResponseDTO();
    const { id_distribution } = request.query;
    const dataResponse:any[] = await getDistributionOpinions(relathionShipRepository,userRepository,id_distribution);
    
    if(dataResponse !== null){
        apiResponse.setSuccesQuery(dataResponse,'Opinions founded');
        response.status(200).json(apiResponse);
    }else{
        apiResponse.setFailQuery(dataResponse);
        response.status(404).json(apiResponse);
    }
});