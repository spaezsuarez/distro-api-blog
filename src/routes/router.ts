import { Application,json,Response } from 'express';
import { authRouter } from './auth_router';
import { distribution_router } from './distribution_router';
import { relation_ship } from './distributions_router';

export const setRoutes = (server:Application):void => {

    server.use(json());
    server.use((_, response:Response, next) => {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
        );
        response.setHeader(
            'Access-Control-Allow-Methods',
            'GET, POST, PUT, DELETE, PATCH, OPTIONS'
        );
        next();
    });

    server.use('/auth',authRouter);
    server.use('/distributions',distribution_router);
    server.use('/user-distributions',relation_ship);
}