import { Router,Request,Response } from 'express';
import { UserRepository } from '../database/UserRepository';
import { userCreationMiddleware } from '../middlewares/user_middleware';  
import { APIResponseDTO } from '../models/APIResponseDTO';
import { authorizedUserProfile,verifyPassword } from '../security/securityParser';
import { User } from '../models/User';
import { serverLogger } from '../config/logger_config';
import { obfuscate } from '../util/util';
import { userAuthenticationMiddleware } from '../middlewares/user_middleware';

export const authRouter:Router = Router();
const userRepository:UserRepository = new UserRepository();

authRouter.post('/sign-up',userCreationMiddleware,(request:Request,response:Response) => {
    const responseData:APIResponseDTO<User> = new APIResponseDTO();
    serverLogger.info(`Inicia Servicio de registro de usuario [${request.body.user.getName()}]`);
    userRepository.save(request.body.user).then(() => {
        serverLogger.info(`Registro de ${request.body.user.getName()} exitoso`);
        responseData.setSuccesQuery(request.body.user,'Registro de usuario exitoso');
        response.status(201).json(responseData);
    }).catch((err) => {
        serverLogger.info(`Error: ${err.message}`);
        responseData.setFailQuery(err);
        response.status(500).json(responseData);
    });
});

authRouter.post('/login',async (request:Request,response:Response) => {
    let responseData:APIResponseDTO<any> = new APIResponseDTO();
    const { email,password_blog } = request.body;

    serverLogger.info(`Inicia Servicio de login [${email}] - [${obfuscate(password_blog)}]`);

    let userData:User = await userRepository.findBy('email',email);
    const verifiedPassword:boolean = (!userData.getIdUser())? false : await verifyPassword(password_blog,userData.getPassword());

    if(verifiedPassword){
        serverLogger.info(`Login para el usuario ${userData.getName()} exitoso`);

        const token:string = await authorizedUserProfile({id_user:userData.getIdUser()});
        responseData.setSuccesQuery({
            token
        },`${userData.getName()} Authenticated`);
        response.status(200).json(responseData);
    }else{
        serverLogger.error('Error: Nombre de usuario y/o contraseña incorrectos');

        responseData.setFailQuery({
            message:'Nombre de usuario y/o contraseña incorrectos'
        });
        response.status(401).json(responseData);
    }
});

authRouter.post('/verify-session', userAuthenticationMiddleware,(request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<any> = new APIResponseDTO();
    apiResponse.setSuccesQuery({isAuthenticated:true},'User Authenticated');
    response.status(200).json(apiResponse);
});
