import { Router,Request,Response } from 'express';
import { LinuxDistributionRepository } from '../database/LinuxDistributionRepository';
import { DistributionLinux } from '../models/DistributionLinux';
import { APIResponseDTO } from '../models/APIResponseDTO';
import { userAuthenticationMiddleware } from '../middlewares/user_middleware';
import { distributionCreationMiddleware } from '../middlewares/creation_middlewares';
import { serverLogger } from '../config/logger_config';

export const distribution_router:Router = Router();
let distributionRepository:LinuxDistributionRepository = new LinuxDistributionRepository();

distribution_router.post('/create',userAuthenticationMiddleware,distributionCreationMiddleware,(request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<DistributionLinux[]> = new APIResponseDTO();
    distributionRepository.save(request.body.distribution).then((result) => {
        apiResponse.setSuccesQuery(result,'Registro guardado exitosamente');
        response.status(201).json(apiResponse);
    }).catch((err) => {
        apiResponse.setFailQuery(err);
        apiResponse.setMessage('Ha ocurrido un error');
        response.status(500).json(apiResponse);
    });

});

distribution_router.get('/all',userAuthenticationMiddleware,(request:Request,response:Response) => {
    const apiResponse:APIResponseDTO<DistributionLinux[]> = new APIResponseDTO();
    distributionRepository.getAll().then((responseData:DistributionLinux[]) => {
        apiResponse.setSuccesQuery(responseData,'List of distributions founded');
        response.status(200).json(apiResponse);
    }).catch((err) => {
        apiResponse.setFailQuery([]);
        apiResponse.setMessage(err.message);
        response.status(404).json(apiResponse);
    });
});

distribution_router.get('/',userAuthenticationMiddleware,async (request:Request,response:Response) => {
    serverLogger.info(`Inicia servicio de consulta para distribución linux con id:${request.query.id_distribution}`);

    const apiResponse:APIResponseDTO<DistributionLinux> = new APIResponseDTO();
    const dataResponse:DistributionLinux = await distributionRepository.findBy('id_distribution',request.query.id_distribution);
    
    serverLogger.info(`Finaliza servicio de consulta para distribución linux`);
    
    if(dataResponse){
        serverLogger.info('Distribution Founded');
        apiResponse.setSuccesQuery(dataResponse,'Distribution Founded');
        response.status(200).json(apiResponse);
    }else{
        serverLogger.info('Distribution Not Found');
        apiResponse.setFailQuery(null);
        apiResponse.setMessage('Distribution Not Found');
        response.status(404).json(apiResponse);
    }
});