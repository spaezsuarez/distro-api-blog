import { Request,Response } from 'express';
import { DistributionLinux } from '../models/DistributionLinux';
import { UserDistributionRelationship } from '../models/User_Distribution';

export const distributionCreationMiddleware = (request:Request,_:Response,next) => {
    const linuxDistribution:DistributionLinux = new DistributionLinux({...request.body});
    request.body.distribution = linuxDistribution;
    next();
}

export const relationShipCreationMiddleware = (request:Request,_:Response,next) => {
    const relationShipData:UserDistributionRelationship = new UserDistributionRelationship({...request.body});
    relationShipData.setIdUser(request.body.id_user);
    request.body.relation_ship = relationShipData;
    next();
}