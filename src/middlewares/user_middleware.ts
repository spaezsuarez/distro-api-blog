import { Response, Request } from 'express';
import { decodePayload, encodePass } from '../security/securityParser';
import { APIResponseDTO } from '../models/APIResponseDTO';
import { User } from '../models/User';
import { serverLogger } from '../config/logger_config';
import { obfuscate } from '../util/util';

export const userCreationMiddleware = async (request:Request, response:Response,next:any):Promise<void> => {
    let user:User = new User({...request.body});
    user.setPassword(await encodePass(user.getPassword()));
    request.body.user = user;
    next();
}

export const userAuthenticationMiddleware = async (request:Request,response:Response,next:any):Promise<void> => {
    const apiResponse: APIResponseDTO<any> = new APIResponseDTO();
    const token = request.headers.authorization || '';
    serverLogger.info('\n-----------------------------------------------------');
    try {
        const { id_user, exp } = await decodePayload(token.split(' ')[1]);
        if(Date.now() > exp*1000){
            serverLogger.info(`Token Expired`);
            apiResponse.setFailQuery({isAuthorized: false });
            apiResponse.setMessage('Token Expired');
            response.status(401).json(apiResponse);
        } else {
            serverLogger.info(`id_user:${ obfuscate(id_user,8) } authenticathed`);
            request.body.id_user = id_user;
            next();
        }
    } catch (error) {
        serverLogger.info(`Error: ${error.message}`);
        apiResponse.setFailQuery({ message:error.message, isAuthorized: false });
        response.status(401).json(apiResponse);
    }
}

export const userVerificationMiddleware = async (request:Request,response:Response,next:any):Promise<void> => {
    const { password } = request.body;
    if(typeof password === 'string') {
        request.body.pass = await encodePass(password);
    } else {
        delete request.body.pass;
    }
    next();
}