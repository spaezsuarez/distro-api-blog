create database distri-api-blog;

create table UserBlog(
    id_user varchar(200) primary key  not null,
    username varchar(50) not null,
    password_blog varchar(100) not null,
    email varchar(100) unique not null
);

create table LinuxDistribution(
	id_distribution varchar(200) PRIMARY key NOT null,
	name_distribution varchar(200) not null,
	description varchar  not null,
	summary varchar  not null,
	official_url varchar not null,
	architecture varchar(200) not null,
	logo varchar(200) not null
);

create table UserBlog_LinuxDistribution(
    id_user varchar(200) not null,
    id_distribution varchar(200) not null,
    opinion varchar(1000)
);

alter table UserBlog_LinuxDistribution add constraint pk_UserBlog_LinuxDistribution primary key (id_user, id_distribution);
alter table UserBlog_LinuxDistribution add constraint fk_UserBlog_LinuxDistribution_User foreign key (id_user) references UserBlog (id_user);
alter table UserBlog_LinuxDistribution add constraint fk_UserBlog_LinuxDistribution_Linux foreign key (id_distribution) references LinuxDistribution (id_distribution);


create table Post(
	id_post varchar(200) primary key not null,
	title varchar(150) not null,
	content_post varchar(500) not null,
	date_publication date not null,
	id_user varchar(200) not null
);

alter table Post add constraint fk_post_user foreign key (id_user) references UserBlog (id_user);

create table Comentary(
	id_comentary varchar(200) primary key not null,
	content_comentary varchar(500) not null,
	date_publication date not null,
	id_post varchar(200) not null,
	id_user varchar(200) not null
);

alter table Comentary add constraint fk_commentary_user foreign key (id_user) references UserBlog (id_user);
alter table Comentary add constraint fk_commentary_post foreign key (id_post) references Post (id_post);