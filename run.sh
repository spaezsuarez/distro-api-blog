#!/bin/bash

opcion=$1
package_manager=$2

clear

if [[ $opcion != dev && $opcion != prod ]]
then
    echo -e "\nIngrese una opcion valida\n"
else
    if [[ $package_manager == npm || $package_manager == yarn ]]
    then
        $package_manager install
        clear
        export $(cat .env)
        export NODE_ENV=$opcion
        if [[ $opcion = dev ]]
        then
            $package_manager run dev
        elif [[ $opcion = prod ]]
        then
            $package_manager start
        fi
    else
        echo -e "\nIngrese un gestor de paquetes valido\n"
    fi
fi