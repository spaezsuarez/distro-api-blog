# Distros-blog-api

## Description
API para un blog de opinión sobre distribuciones linux, presentando información basica de ellas

## Author

- Sergio David Paez Suarez - 20191020167
## Installation

Para el uso de la aplicación es necesario tener instalado algun gestor de paquetes para nodejs como lo pueden ser `npm` o `yarn`, y posteriormente ejecutar el siguiente comando

```console
npm install

yarn install
```
## Usage

La ejecución del proyecto se puede realizar mediante la siguiente instrucción

```console
yarn start o  npm start

yarn dev o npm run dev
```
O lo puede hacer ejecutando el script de bash que se encuentra en el proyecto (`run.sh`)

```console
bash run.sh <option> <package_manager>
```
`option` debe ser dev o prod
`package_manager` debe ser yarn o npm

tambien debe asegurarse de tener las siguientes variables de entorno configuradas en un archivo `.env`

```
DB_USER=
DB_PASSWORD=
DB_HOST=
DB_PORT=
DB_NAME=
TOKEN_SECRET=
SALTS=
ALGORITHM=HS256
SECRET_HASH=
EXPIRATION_TIME=
PORT=
```

## Postman

- En la carpeta `schema` se encuentra archivo json, el cual representa una colección de postman en caso de que se
desee probar la api `API Distro Blog.postman_collection.json`

## License

 GNU Affero General Public License

